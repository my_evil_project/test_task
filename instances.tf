###########################################
#creates two ec2 instances in different vpc, and rds in primary
###########################################
resource "aws_key_pair" "test" {
  key_name   = "test"
  public_key = var.ssh_key
}

resource "aws_instance" "primary-az1" {
  instance_type          = var.instance_class
  ami                    = var.ami_id
  key_name               = aws_key_pair.test.id
  subnet_id              = aws_subnet.primary-az1.id
  vpc_security_group_ids = [aws_security_group.primary-default.id]
  tags = {
    Name = "primary-az1"
  }
}


resource "aws_instance" "secondary-az1" {
  instance_type          = var.instance_class
  ami                    = var.ami_id
  key_name               = aws_key_pair.test.id
  subnet_id              = aws_subnet.secondary-az1.id
  vpc_security_group_ids = [aws_security_group.secondary-default.id]
  tags = {
    Name = "secondary-az1"
  }
}

resource "aws_db_instance" "db" {
  identifier             = "db"
  engine                 = "mysql"
  engine_version         = "5.7"
  port                   = "3306"
  name                   = "test"
  username               = "test"
  password               = "testpass"
  instance_class         = "db.t2.micro"
  allocated_storage      = 5
  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.db.id
  vpc_security_group_ids = [aws_security_group.db.id]
  publicly_accessible    = false
}
