###########################################
#   Create policy (lambda need ec2 Networking Actions)
#   Create IAM role for lambda Service
#   Attachment policy in AIM role
#   Create Lambda function
###########################################

resource "aws_iam_policy" "policy-lambda" {
  name        = "policy-lambda"
  description = "A test policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:DescribeNetworkInterfaces",
        "ec2:CreateNetworkInterface",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeInstances",
        "ec2:AttachNetworkInterface"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role" "lambda-role" {
  name = "lambda-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.lambda-role.name
  policy_arn = aws_iam_policy.policy-lambda.arn
}
resource "aws_lambda_function" "test_lambda" {
  filename      = "files/lambda.zip"
  function_name = "lambda_test"
  role          = aws_iam_role.lambda-role.arn
  handler       = "lambda_function.lambda_handler"
  runtime       = "python2.7"

  vpc_config  {
    subnet_ids       = [ aws_subnet.primary-az1.id ]
    security_group_ids = [ aws_security_group.primary-default.id ]
  }

  tags = {
    Environment = "test"
  }
}
