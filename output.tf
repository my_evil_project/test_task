###########################################
##   Outputs for s3
###########################################
output "this_s3_regional_bucket_name" {
  description = "The regional name of the bucket."
  value       = aws_s3_bucket.site-s3-test.bucket_regional_domain_name
}

output "this_s3_bucket_name" {
  description = "The name of the bucket."
  value       = aws_s3_bucket.site-s3-test.bucket_domain_name
}

output "this_s3_bucket_endpoint" {
  description = "The endpoint of the bucket."
  value       = aws_s3_bucket.site-s3-test.website_endpoint
}

output "user_arn" {
  description = "The arn of the user."
  value = aws_iam_user.s3-user.arn
}

output "user_name" {
  description = "The user name of the user."
  value = aws_iam_user.s3-user.name
}

output "bucket_arn" {
  description = "The bucket arn of the bucket."
  value = aws_s3_bucket.site-s3-test.arn
}

output "iam_access_key_id" {
  description = "The iam access key id of the user."
  value = aws_iam_access_key.user_keys.id
}
output "iam_secret_key" {
  description = "The iam secret key id of the user."
  value = aws_iam_access_key.user_keys.secret
}

###########################################
#   Outputs for CloudFront
###########################################
output "cf_arn" {
  description = "ARN of AWS CloudFront distribution"
  value       = aws_cloudfront_distribution.s3-cf.arn
}

output "cf_status" {
  description = "Current status of the distribution"
  value       = aws_cloudfront_distribution.s3-cf.status
}

output "cf_domain_name" {
  description = "Domain name corresponding to the distribution"
  value       = aws_cloudfront_distribution.s3-cf.domain_name
}

output "cf_etag" {
  description = "Current version of the distribution's information"
  value       = aws_cloudfront_distribution.s3-cf.etag
}

output "cf_hosted_zone_id" {
  description = "CloudFront Route 53 zone ID"
  value       = aws_cloudfront_distribution.s3-cf.hosted_zone_id

}

output "cf_id" {
  description = "ID of AWS CloudFront distribution"
  value       = aws_cloudfront_distribution.s3-cf.id
}

