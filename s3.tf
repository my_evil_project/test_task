###########################################
#   Create IAM user (need fot deploy)
#   Create policy to use the CloudFront console
#   Attach policy with all grant to s3
#   Gateway needed for plan
#   Create S3 bucket
###########################################
resource "aws_iam_user" "s3-user" {
  name = "s3-user"
  force_destroy = "true"
}

resource "aws_iam_user_policy" "cf_policy" {
  name = "cf_policy"
  user = aws_iam_user.s3-user.name

  policy = <<EOF
{
   "Version": "2012-10-17",
   "Statement":[
      {
         "Effect":"Allow",
         "Action":[
            "acm:ListCertificates",
            "cloudfront:*",
            "cloudwatch:DescribeAlarms",
            "cloudwatch:PutMetricAlarm",
            "cloudwatch:GetMetricStatistics",
            "elasticloadbalancing:DescribeLoadBalancers",
            "iam:ListServerCertificates",
            "sns:ListSubscriptionsByTopic",
            "sns:ListTopics",
            "waf:GetWebACL",
            "waf:ListWebACLs"
         ],
         "Resource":"*"
      },
      {
         "Effect":"Allow",
         "Action":[
            "s3:ListAllMyBuckets",
            "s3:PutBucketPolicy"
         ],
         "Resource":"arn:aws:s3:::*"
      }
   ]
}
EOF
}

resource "aws_iam_user_policy_attachment" "test-attach" {
  user       = aws_iam_user.s3-user.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_access_key" "user_keys" {
  user = aws_iam_user.s3-user.name
}

resource "aws_s3_bucket" "site-s3-test" {
  bucket = "site-s3-test"
  force_destroy = "true"
  acl = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
  policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [{
		"Sid": "PublicReadForGetBucketObjects",
		"Effect": "Allow",
		"Principal": "*",
		"Action": "s3:GetObject",
		"Resource": "arn:aws:s3:::site-s3-test/*"
	}]
}
EOF
}

