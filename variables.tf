variable "region" {
  default = "eu-central-1"
}

variable "ami_id" {
  default = "ami-03ab4e8f1d88ce614"
}

variable "instance_class" {
  default = "t2.micro"
}

variable "ssh_key" {
  description = "enter ssh key pub"
  type        = string
}
