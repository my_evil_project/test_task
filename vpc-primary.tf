###########################################
#   Create VPC 1172.30.0.0/16 "primary"
#   Create two Subnet in other availability zone
#   Subnet group for db (need two subnet in other availability zone)
#   Gateway needed for plan
#   S-group for ssh and incoming icmp
#   S-group for db to incoming 3306 port
###########################################
resource "aws_vpc" "primary" {
  cidr_block = "172.30.0.0/16"
  tags = {
    Name = "primary"
  }
}

resource "aws_internet_gateway" "primary" {
  vpc_id = aws_vpc.primary.id
}

resource "aws_route" "primary-internet_access" {
  route_table_id         = aws_vpc.primary.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.primary.id
}

resource "aws_subnet" "primary-az1" {
  vpc_id                  = aws_vpc.primary.id
  cidr_block              = "172.30.131.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "eu-central-1a"
}

resource "aws_subnet" "primary-az2" {
  vpc_id                  = aws_vpc.primary.id
  cidr_block              = "172.30.132.0/24"
  availability_zone       = "eu-central-1b"
}


resource "aws_db_subnet_group" "db" {
  name       = "db subnet"
  subnet_ids = [aws_subnet.primary-az1.id, aws_subnet.primary-az2.id]
}

resource "aws_security_group" "primary-default" {
  name_prefix = "default-"
  description = "Default security group for all instances in VPC"
  vpc_id      = aws_vpc.primary.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["10.0.1.0/24"]
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["172.30.131.0/24"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "db" {
  name   = "db security group" 
  vpc_id = aws_vpc.primary.id
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["172.30.131.0/24", "172.30.132.0/24"]
  }
}

