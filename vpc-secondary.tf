###########################################
#   Create VPC 10.0.0.0/16
#   Gateway only for ssh to checking ping
#   S-group for ssh
###########################################

resource "aws_vpc" "secondary" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "secondary"
  }
}

resource "aws_internet_gateway" "secondary" {
  vpc_id = aws_vpc.secondary.id
}

resource "aws_route" "secondary-internet_access" {
  route_table_id         = aws_vpc.secondary.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.secondary.id
}

resource "aws_subnet" "secondary-az1" {
  vpc_id                  = aws_vpc.secondary.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "eu-central-1b"
}

resource "aws_security_group" "secondary-default" {
  name_prefix = "default-"
  description = "Default security group for all instances in VPC"
  vpc_id      = aws_vpc.secondary.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
